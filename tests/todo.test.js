const app = require("../app")
const request = require('supertest')

describe("GET /todos", () => {

    it("test get /todos", (done) => {
        request(app)
        .get("/todos")
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const firstData = response.body[0]
            expect(firstData.type).toBe('urgent')
            expect(firstData.title).toBe('Belajar')
            expect(firstData.status).toBe('active')
            done()
        })
        .catch(done)
    })

    it("test get /todos/:id", (done) => {
        request(app)
        .get("/todos/2")
        .expect('Content-Type', /json/)
        .expect(200)
        .then(response => {
            const data = response.body
            expect(data.id).toBe(2)
            expect(data.title).toBe('Ngegitar')
            expect(data.status).toBe('active')
            expect(data.type).toBe('urgent')
            done()
        })
        .catch(done)
    })

    it("test post /todos", (done) => {
        
        request(app)
            .post("/todos")
            .send({title: 'Mantap2', status: "active", type: "urgent"})
            .set('Accept', 'application/json')
            .expect('Content-Type', /json/)
            .expect(201)
            .then(response => {
                const data = response.body;
                expect(data.title).toBe("Mantap2")
                expect(data.status).toBe("active")
                expect(data.type).toBe("urgent")
                done()
            })
        .catch(done)
    })

    it("delete post /todos/:id", (done) => {
        
        request(app)
            .delete("/todos/3")
            .expect('Content-Type', /json/)
            .expect(200)
            .then(response => {
                const data = response.body;
                expect(data).toStrictEqual({
                    message: "Todo deleted"
                })
                done()
            })
        .catch(done)
    })
    
});
