'use strict';

const date = new Date()

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, Sequelize) {
    await queryInterface.bulkInsert('Todos', [{
      title: "Belajar",
      status: "active",
      type: "urgent",
      createdAt: date,
      updatedAt: date
    },
    {
      title: "Ngegitar",
      status: "active",
      type: "urgent",
      createdAt: date,
      updatedAt: date
    },
    {
      title: "Joged",
      status: "active",
      type: "urgent",
      createdAt: date,
      updatedAt: date
    },
    {
      title: "Ngegame",
      status: "active",
      type: "urgent",
      createdAt: date,
      updatedAt: date
    },
    {
      title: "Bikin HW",
      status: "active",
      type: "urgent",
      createdAt: date,
      updatedAt: date
    }])
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
    */
  },

  async down (queryInterface, Sequelize) {
    await queryInterface.bulkDelete('Todos', null, {})
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  }
};
