const {Todo} = require("../models")

class TodosController {

    static findAll = async(req, res, next) => {

        try {
            const data = await Todo.findAll({
                where: { 
                    status: "active" },
                order:[
                    ["id", "asc"]
                ]
            });
            
            res.status(200).json(data);
            
        } catch (err) {
            next(err)
            
        }
        
    }

    static findOne = async (req, res, next) => {
        const { id } = req.params;
        try {
          const todo = await Todo.findByPk(id);
          if (!todo) {
            return res.status(404).send('Todo not found');
          }
          res.status(200).json(todo);
        } catch (err) {
          next(err);
        }
    }
    
    static create = async (req, res, next) => {
        try {
          const { title, status, type } = req.body;

          const newTodo = await Todo.create({
            title,
            status,
            type
          });
      
          res.status(201).json(newTodo);
        } catch (err) {
          next(err);
        }
    }
      
    static destroy = async (req, res, next) => {
        try {
            const {id} = req.params;
    
            const data = await Todo.update(
                { status: "inactive" },
                { where: { id } }
            );
    
            if (data[0] === 0) {
                throw {name: "ErrorNotFound"}
                
            } else {
                res.status(200).json({ message: "Todo deleted" });
            }
        } catch (err) {
            next(err)
        }
    }
    

    

}

module.exports = TodosController;