'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  up: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Todos', 'name', 'title');
  },

  down: async (queryInterface, Sequelize) => {
    await queryInterface.renameColumn('Todos', 'title', 'name');
  }
};
