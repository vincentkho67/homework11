const express = require("express");
const router = express.Router();
const TodosController = require("../controllers/todosController");

router.get("/todos", TodosController.findAll);
router.get("/todos/:id", TodosController.findOne);
router.post("/todos", TodosController.create);
router.delete("/todos/:id", TodosController.destroy);
module.exports = router;